<?php
defined('TYPO3_MODE') or die();

$extKey = 'hive_cpt_cnt_bs_carousel';
$title = '[HIVE] Bootstrap Carousel';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($extKey, 'Configuration/TypoScript', $title);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivecptcntbscarousel_domain_model_carousel', 'EXT:hive_cpt_cnt_bs_carousel/Resources/Private/Language/locallang_csh_tx_hivecptcntbscarousel_domain_model_carousel.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivecptcntbscarousel_domain_model_carousel');