<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveCptCntBsCarousel',
            'Hivecptcntbscarouselcarouselshowcarousel',
            [
                'Carousel' => 'renderCarousel'
            ],
            // non-cacheable actions
            [
                'Carousel' => ''
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hivecptcntbscarouselcarouselshowcarousel {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('hive_cpt_cnt_bs_carousel') . 'Resources/Public/Icons/user_plugin_hivecptcntbscarouselcarouselshowcarousel.svg
                        title = LLL:EXT:hive_cpt_cnt_bs_carousel/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_bs_carousel_domain_model_hivecptcntbscarouselcarouselshowcarousel
                        description = LLL:EXT:hive_cpt_cnt_bs_carousel/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_bs_carousel_domain_model_hivecptcntbscarouselcarouselshowcarousel.description
                        tt_content_defValues {
                            CType = list
                            list_type = hivecptcntbscarousel_hivecptcntbscarouselcarouselshowcarousel
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

call_user_func(
    function($extKey)
    {

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins.elements.hivecptcntbscarouselcarouselshowcarousel >
                wizards.newContentElement.wizardItems {
                    hive {
                        header = Hive
                        after = common,special,menu,plugins,forms
                        elements.hivecptcntbscarouselcarouselshowcarousel {
                            iconIdentifier = bootstrap_cpt_brand_32x32_svg
                            title = Bootstrap Carousel
                            description = Carousel without Flux
                            tt_content_defValues {
                                CType = list
                                list_type = hivecptcntbscarousel_hivecptcntbscarouselcarouselshowcarousel
                            }
                        }
                        show := addToList(hivecptcntbscarouselcarouselshowcarousel)
                    }

                }
            }'
        );

    }, $_EXTKEY
);