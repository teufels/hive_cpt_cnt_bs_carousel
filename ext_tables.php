<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveCptCntBsCarousel',
            'Hivecptcntbscarouselcarouselshowcarousel',
            'hive_cpt_cnt_bs_carousel :: Carousel :: showCarousel'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_cpt_cnt_bs_carousel', 'Configuration/TypoScript', 'hive_cpt_cnt_bs_carousel');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivecptcntbscarousel_domain_model_carousel', 'EXT:hive_cpt_cnt_bs_carousel/Resources/Private/Language/locallang_csh_tx_hivecptcntbscarousel_domain_model_carousel.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivecptcntbscarousel_domain_model_carousel');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder