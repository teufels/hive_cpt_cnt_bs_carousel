/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Andreas Hafner, Dominik Hilser, Georg Kathan, Hendrik Krüger, Timo Bittner :: teufels GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 **/

;(function ( $, window, document, undefined ) {

    var bIsPreloading = false;

    "use strict";

    // undefined is used here as the undefined global variable in ECMAScript 3 is
    // mutable (ie. it can be changed by someone else). undefined isn't really being
    // passed in so we can ensure the value of it is truly undefined. In ES5, undefined
    // can no longer be modified.

    // window and document are passed through as local variable rather than global
    // as this (slightly) quickens the resolution process and can be more efficiently
    // minified (especially when both are regularly referenced in your plugin).

    // Create the defaults once
    var pluginName = "bootstrapCarouselLazy",
        defaults = {
            propertyName: "value",
            defaultSelector: ".carousel-fade",
            bIsPreloading: false
        };

    // The actual plugin constructor
    function Plugin ( element, options ) {
        this.element = element;
        // jQuery has an extend method which merges the contents of two or
        // more objects, storing the result in the first object. The first object
        // is generally empty as we don't want to alter the default options for
        // future instances of the plugin
        this.settings = $.extend( {}, defaults, options );
        this._defaults = defaults;
        this._name = pluginName;

        this.init();

    }

    // Avoid Plugin.prototype conflicts
    $.extend(Plugin.prototype, {
        init: function () {
            // Place initialization logic here
            // You already have access to the DOM element and
            // the options via the instance, e.g. this.element
            // and this.settings
            // you can add more functions like the one below and
            // call them like so: this.yourOtherFunction(this.element, this.settings).
            if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
                console.log("xD");
            }

            window.oCarousel = new Object();
            window.oCarouselPreloading = new Object();

            $(this.settings.defaultSelector).each(function(){

                var $oThisCarousel = $(this);
                var $sCarouselId = $oThisCarousel.attr('id');

                // initialize carousel
                $oThisCarousel.carousel({
                    //interval:6000,
                    pause: "false"
                });

                // pause carousel
                $oThisCarousel.carousel('pause');

                // array of items
                if (typeof $sCarouselId === 'undefined') {
                } else {
                    oCarousel[$sCarouselId] = [];
                    oCarouselPreloading[$sCarouselId] = false;
                    $("#" + $sCarouselId + " .item").each(function(){
                        var $oItem = $(this);
                        var sT = "img";
                        var $oImage = $oItem.find('img');
                        if ($oImage.length == 0) {
                          $oImage = $oItem.find('div.focuhila');
                          sT = "div";
                        }
                        var $sImageId = $oImage.attr('id');
                        var $sImageDataSrc = $oImage.attr('data-echo');
                        if (typeof $sImageId === 'undefined' && typeof $sImageDataSrc === 'undefined') {
                        } else {
                            var i = oCarousel[$sCarouselId].length;
                            oCarousel[$sCarouselId][i] = new Object();
                            oCarousel[$sCarouselId][i]["sT"] = sT;
                            oCarousel[$sCarouselId][i]["oImage"] = $oImage;
                            oCarousel[$sCarouselId][i]["id"] =  $sImageId;
                            oCarousel[$sCarouselId][i]["data-echo"] =  $sImageDataSrc;
                            oCarousel[$sCarouselId][i]["bLoaded"] = false;
                        }
                    });

                    if ($('body').hasClass('bMobile-1') && !$('body').hasClass('win') && !$('body').hasClass('phone') && typeof Hammer != 'undefined' && typeof Hammer == 'function') {

                        var myElement = document.getElementById($sCarouselId);
                        // create a simple instance
                        // by default, it only adds horizontal recognizers
                        var mc = new Hammer(myElement);
                        // listen to events...
                        mc.on("swipeleft swiperight", function(ev) {
                            if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
                                console.info(ev.type + " gesture detected.");
                            }
                            //myElement.textContent = ev.type +" gesture detected.";
                            var iActiveItem = $oThisCarousel.find('.item.active').index();
                            var iLastItem = $oThisCarousel.find('.item:last-child').index();
                            if ((iActiveItem > 0 || oCarousel[$sCarouselId][iLastItem]["bLoaded"]) && ev.type == 'swiperight') {
                                $oThisCarousel.carousel('prev');
                            }
                            if ((ev.type == 'swipeleft' || ev.type == 'panleft')) {
                                $oThisCarousel.carousel('next');
                            }
                        });

                    }

                    $oThisCarousel.on('click', 'a.left.carousel-control', function(event) {
                        event.preventDefault();
                        event.stopPropagation();
                        var iActiveItem = $oThisCarousel.find('.item.active').index();
                        var iLastItem = $oThisCarousel.find('.item:last-child').index();
                        if (iActiveItem > 0 || oCarousel[$sCarouselId][iLastItem]["bLoaded"]) {
                            $oThisCarousel.carousel('prev');
                        }
                    });
                    $oThisCarousel.on('click', 'a.right.carousel-control', function(event) {
                        event.preventDefault();
                        event.stopPropagation();
                        $oThisCarousel.carousel('next');

                    });

                }

            });

            // if (Object.keys(oCarousel).length > 0) {
// 
//                 // each carousel
//                 for (var key in oCarousel) {
// 
//                     if (Object.keys(oCarousel[key]).length > 0 ) {
// 
//                         // preload first item image
//                         this.preloadSlide(key, 0);
// 
//                         this.focusSlide(key, 0);
// 
//                         this.showSlide(key, 0);
// 
//                         // preload second item image
//                         if (Object.keys(oCarousel[key]).length > 1 ) {
//                             this.preloadSlide(key, 1);
// 
//                         }
// 
//                     }
// 
//                 }
// 
//             }
            
            this.preloadSlider();

            if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
                console.log(oCarousel);
                console.log(Object.keys(oCarousel).length);
            }

            //
            var CustomHiveCarousel_scrollTimeout;  // global for any pending scrollTimeout
            var $cf = $(this.settings.defaultSelector);
            if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
                console.log('$cf.length ' + $cf.length);
            }

            if ($cf.length > 0) {
                $(window).scroll(function () {
                    if (CustomHiveCarousel_scrollTimeout) {
                        // clear the timeout, if one is pending
                        clearTimeout(CustomHiveCarousel_scrollTimeout);
                        CustomHiveCarousel_scrollTimeout = null;
                    }
                    CustomHiveCarousel_scrollTimeout = setTimeout(CustomHiveCarousel_scrollHandler, 1000);
                });

                var $myThis;
                $myThis = this;
                CustomHiveCarousel_scrollHandler = function () {
                    if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
                        console.log('$cf'); console.log($cf);
                    }
                    for (var i = 0; i < $cf.length; i++ ) {
                        if ($($cf[i]).isOnScreen() && !oCarouselPreloading[$($cf[i]).attr('id')]) {
                            if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
                                console.log('loadSlider() -> ' + $($cf[i]).attr('id'));
                            }
                            $myThis.preloadSpecificSlider($($cf[i]).attr('id'));
                        } else {
                            if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
                                console.log($($cf[i]).attr('id') + '.carousel(pause)');
                            }
                            $($cf[i]).carousel('pause');
                        }
                    }

                };
            }
            //

            var pThis = this;
            $(this.settings.defaultSelector).on('slid.bs.carousel', function () {
                var $oThisCarousel = $(this);
                var $sCarouselId = $oThisCarousel.attr('id');

                if (typeof $sCarouselId === 'undefined') {
                } else {

                    var iActiveItem = $(this).find('.item.active').index();

                    if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
                        console.log($sCarouselId + ' -> iActiveItem -> ' + iActiveItem);
                    }

                    if (iActiveItem == 0 || iActiveItem == Object.keys(oCarousel[$sCarouselId]).length -1 ) {
                        // do not stop
                    } else {
                        // check if next item is already loaded
                        var iNextElement = iActiveItem -0 + 1;
                        pThis.preloadSlide($sCarouselId, iNextElement);
                    }


                    // show current slide
                    pThis.focusSlide($sCarouselId, iActiveItem);
                    pThis.showSlide($sCarouselId, iActiveItem);

                }

            });

            $(this.settings.defaultSelector).on('click', '.carousel-indicators li.disabled', function(){
                return false;
            });

        },
        preloadSlider: function () {
            if (Object.keys(oCarousel).length > 0) {

                // each carousel
                for (var key in oCarousel) {

                    if (Object.keys(oCarousel[key]).length > 0 ) {

                        if ($("#" + key).isOnScreen()) {
                            // preload first item image
                            this.preloadSlide(key, 0);

                            this.focusSlide(key, 0);

                            this.showSlide(key, 0);

                            // preload second item image
                            if (Object.keys(oCarousel[key]).length > 1 ) {
                                this.preloadSlide(key, 1);
                            }
                        }

                    }

                }

            }
        },
        preloadSpecificSlider: function (sCarouselId) {
            if (Object.keys(oCarousel).length > 0) {

                // each carousel
                for (var key in oCarousel) {

                    if (Object.keys(oCarousel[key]).length > 0 ) {

                        if ($("#" + key).isOnScreen() && key == sCarouselId) {
                            // preload first item image
                            this.preloadSlide(key, 0);

                            this.focusSlide(key, 0);

                            this.showSlide(key, 0);

                            // preload second item image
                            if (Object.keys(oCarousel[key]).length > 1 ) {
                                this.preloadSlide(key, 1);
                            }
                        }

                    }

                }

            }
        },
        preloadSlide: function (sCarouselId, iItemIndex) {

            var $sCarouselId = sCarouselId;
            
            if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
                console.log($sCarouselId + ' - ' + iItemIndex + ' -> img ' + oCarousel[$sCarouselId][iItemIndex]["id"] + ' loading!');
            }

            if (!oCarousel[$sCarouselId][iItemIndex]["bLoaded"]) {
                oCarouselPreloading[$sCarouselId] = true;
                // stop
                $("#" + $sCarouselId).carousel('pause');

                // preload item image
                oCarousel[$sCarouselId][iItemIndex]["oImage"][0].onload = function () {

                    // set bLoaded
                    oCarousel[$sCarouselId][iItemIndex]["bLoaded"] = true;

                    if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
                        console.log($sCarouselId + ' - ' + iItemIndex + ' -> img ' + oCarousel[$sCarouselId][iItemIndex]["id"] + ' loaded!');
                    }

                    // cycle
                    if ($("#" + $sCarouselId).isOnScreen()) {
                        $("#" + $sCarouselId).carousel('cycle');
                    } else {
                        $("#" + $sCarouselId).carousel('pause');
                    }
                    //$("#" + $sCarouselId).carousel('cycle');

                };
                if (oCarousel[$sCarouselId][iItemIndex]["sT"] == "img") {
                  oCarousel[$sCarouselId][iItemIndex]["oImage"][0].src = oCarousel[$sCarouselId][iItemIndex]["data-echo"];
                } else {
                  oCarousel[$sCarouselId][iItemIndex]["oImage"].css("background-image",'url("' + oCarousel[$sCarouselId][iItemIndex]["data-echo"] + '")');
                }

                $("#" + $sCarouselId + " li[data-slide-to='" + iItemIndex + "']").removeClass('disabled');
                oCarousel[sCarouselId][iItemIndex]["oImage"].parent().parent().addClass('imgLoaded');

                oCarouselPreloading[$sCarouselId]  = false;
            }
        },
        focusSlide: function (sCarouselId, iItemIndex) {
            if(oCarousel[sCarouselId][iItemIndex]["oImage"].parent().hasClass('focuspoint')) {
                oCarousel[sCarouselId][iItemIndex]["oImage"].parent().focusPoint().adjustFocus();
            }
        },
        showSlide: function (sCarouselId, iItemIndex) {
            oCarousel[sCarouselId][iItemIndex]["oImage"].addClass('fadeIn');
        }
    });

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[ pluginName ] = function ( options ) {
        return this.each(function() {
            if ( !$.data( this, "plugin_" + pluginName ) ) {
                $.data( this, "plugin_" + pluginName, new Plugin( this, options ) );
            }
        });
    };

})( jQuery, window, document );
