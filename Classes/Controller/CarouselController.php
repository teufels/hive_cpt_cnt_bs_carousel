<?php
namespace HIVE\HiveCptCntBsCarousel\Controller;
/***
 *
 * This file is part of the "hive_cpt_cnt_bs_carousel" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 ***/

/**
 * CarouselController
 */
class CarouselController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * carouselRepository
     *
     * @var \HIVE\HiveCptCntBsCarousel\Domain\Repository\CarouselRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $carouselRepository = null;

    /**
     * action renderCarousel
     *
     * @return void
     */
    public function renderCarouselAction()
    {
        // deprecated
        // @extensionScannerIgnoreLine
        $pluginUid = $this->configurationManager->getContentObject()->data['uid'];

        $aSlidesTemp = $this->settings['slides'];
        $aSlides= array();

        /* get only activ slides */
        foreach ($aSlidesTemp as $slideTemp) {
            if (boolval($slideTemp['slide']['inactiv']) === false) {
                array_push($aSlides,$slideTemp);
            }
        }
        //var_dump($aSlides);
        //var_dump($pluginUid);
        $this->view->assign('recordUid', $pluginUid);
        $this->view->assign('slides', $aSlides);
    }
}
